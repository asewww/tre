#!/usr/bin/env python3
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CommandHandler, CallbackQueryHandler, Updater
from github import Github
from dotenv import load_dotenv
import os
from datetime import datetime, timedelta
from telegram.ext import CallbackContext

load_dotenv(override=True)

# Load environment variables
personal_access_token = os.getenv("GITHUB_PERSONAL_ACCESS_TOKEN")
github_file_path = os.getenv("GITHUB_FILE_PATH")
telegram_bot_token = os.getenv("TELEGRAM_BOT_TOKEN")
allowed_usernames = os.getenv("ALLOWED_USERNAMES").split(",") if os.getenv("ALLOWED_USERNAMES") else []

# Zona WIB
wib = datetime.utcnow() + timedelta(hours=7)

def is_allowed_user(update):
    user = update.message.from_user
    return user.username in allowed_usernames

def handle_unauthorized(update):
    context.bot.send_message(chat_id=update.effective_chat.id, text="You are not allowed to use this command.")

def handle_github_operation(update, operation, file_path, content, commit_message):
    try:
        # Connect to Github API using personal access token with timeout
        g = Github(personal_access_token, timeout=10)

        # Get repository and file objects from Github
        repo = g.get_repo("arunaid/debbb")
        file_path = "ipregist"
        file_content = repo.get_contents(file_path)

        # Perform the requested operation on the file content
        content_decoded = file_content.decoded_content.decode()
        content = operation(content_decoded, content)

        # Commit changes to the Github file
        repo.update_file(file_path, commit_message, content, file_content.sha)
        
        return True
    except Exception as e:
        response_msg = f"An error occurred: {str(e)}"
        context.bot.send_message(chat_id=update.effective_chat.id, text=response_msg)
        return False

def add_ip_operation(existing_content, new_ip_data):
    new_row = f"{new_ip_data[0]}, {new_ip_data[1]}, {new_ip_data[2]}\n"
    return existing_content + new_row

def delete_ip_operation(existing_content, ip_to_delete):
    new_content = ""
    for line in existing_content.splitlines():
        if ip_to_delete not in line:
            new_content += line + "\n"
    return new_content

def renew_ip_operation(existing_content, ip_to_renew):
    new_content = ""
    for line in existing_content.splitlines():
        if ip_to_renew in line:
            parts = line.split(", ")
            parts[2] = ip_to_renew[2]
            line = ", ".join(parts)
        new_content += line + "\n"
    return new_content

def add_ip(update, context):
    if not is_allowed_user(update):
        handle_unauthorized(update)
        return

    message_args = context.args
    if len(message_args) < 3:
        response_msg = "Invalid command format. Use: /add_ip <client_name> <ip_address> <expiration_date>"
        context.bot.send_message(chat_id=update.effective_chat.id, text=response_msg)
        return

    new_ip_data = (message_args[0], message_args[1], message_args[2])

    # Perform the add IP operation
    success = handle_github_operation(update, add_ip_operation, "ipregist", new_ip_data, "Add new IP address")
    
    if success:
        response_msg = f"{new_ip_data[0]} with IP address {new_ip_data[1]} has been successfully added.\nValid until {new_ip_data[2]}."
        context.bot.send_message(chat_id=update.effective_chat.id, text=response_msg)

def delete_ip(update, context):
    if not is_allowed_user(update):
        handle_unauthorized(update)
        return

    message_args = context.args
    if len(message_args) < 2:
        response_msg = "Invalid command format. Use: /delete_ip <client_name> <ip_address>"
        context.bot.send_message(chat_id=update.effective_chat.id, text=response_msg)
        return

    ip_to_delete = (message_args[0], message_args[1])

    # Perform the delete IP operation
    success = handle_github_operation(update, delete_ip_operation, "ipregist", ip_to_delete, "Delete IP address")
    
    if success:
        response_msg = f"IP address {ip_to_delete[1]} owned by {ip_to_delete[0]} has been deleted."
        context.bot.send_message(chat_id=update.effective_chat.id, text=response_msg)

def renew_ip(update, context):
    if not is_allowed_user(update):
        handle_unauthorized(update)
        return

    message_args = context.args
    if len(message_args) < 3:
        response_msg = "Invalid command format. Use: /renew_ip <client_name> <ip_address> <new_expiration_date>"
        context.bot.send_message(chat_id=update.effective_chat.id, text=response_msg)
        return

    ip_to_renew = (message_args[0], message_args[1], message_args[2])

    # Perform the renew IP operation
    success = handle_github_operation(update, renew_ip_operation, "ipregist", ip_to_renew, "Renew IP address")
    
    if success:
        response_msg = f"IP address {ip_to_renew[1]} for {ip_to_renew[0]} has been renewed.\nValid until {ip_to_renew[2]}."
        context.bot.send_message(chat_id=update.effective_chat.id, text=response_msg)

def check_ip_list(update, context):
    if not is_allowed_user(update):
        handle_unauthorized(update)
        return

    try:
        # Connect to Github API using personal access token with timeout
        g = Github(personal_access_token, timeout=10)

        # Get repository and file objects from Github
        repo = g.get_repo("YOUR-GITHUB-USERNAME/YOUR-REPO-NAME")
        file_content = repo.get_contents("ipregist")

        # Get all IP addresses from the file
        file_content_decoded = file_content.decoded_content.decode()
        ip_addresses = []
        for line in file_content_decoded.splitlines():
            parts = line.split(", ")
            ip_addresses.append(f"{parts[0]}: {parts[1]} (expired on {parts[2]})")

        # Send response back to the Telegram user
        response_msg = "List of registered IP addresses:\n\n" + "\n".join(ip_addresses)
        context.bot.send_message(chat_id=update.effective_chat.id, text=response_msg)
    except Exception as e:
        response_msg = f"An error occurred: {str(e)}"
        context.bot.send_message(chat_id=update.effective_chat.id, text=response_msg)

def start(update: Update, context: CallbackContext) -> None:
    keyboard = [
        [InlineKeyboardButton("Tambahkan IP", callback_data='add_ip')],
        [InlineKeyboardButton("Hapus IP", callback_data='delete_ip')],
        [InlineKeyboardButton("Perbarui IP", callback_data='renew_ip')],
        [InlineKeyboardButton("Cek Daftar IP", callback_data='check_ip_list')],
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('Halo! Saya bot Anda yang baru.', reply_markup=reply_markup)

def button_handler(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    query.answer()

    if query.data == 'add_ip':
        add_ip(update, context)
    elif query.data == 'delete_ip':
        delete_ip(update, context)
    elif query.data == 'renew_ip':
        renew_ip(update, context)
    elif query.data == 'check_ip_list':
        check_ip_list(update, context)

def main():
    try:
        # Initialize Telegram bot
        updater = Updater(telegram_bot_token, use_context=True)
        dispatcher = updater.dispatcher

        # Add command handler for "/start"
        start_handler = CommandHandler("start", start)
        dispatcher.add_handler(start_handler)

        # Add callback handler for buttons
        callback_handler = CallbackQueryHandler(button_handler)
        dispatcher.add_handler(callback_handler)

        # Add command handler for "/add_ip"
        add_ip_handler = CommandHandler("add_ip", add_ip)
        dispatcher.add_handler(add_ip_handler)

        # Add command handler for "/delete_ip"
        delete_ip_handler = CommandHandler("delete_ip", delete_ip)
        dispatcher.add_handler(delete_ip_handler)

        # Add command handler for "/renew_ip"
        renew_ip_handler = CommandHandler("renew_ip", renew_ip)
        dispatcher.add_handler(renew_ip_handler)

        # Add command handler for "/check_ip_list"
        check_ip_list_handler = CommandHandler("check_ip_list", check_ip_list)
        dispatcher.add_handler(check_ip_list_handler)

        # Run the bot
        updater.start_polling()
        updater.idle()
    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == '__main__':
    main()